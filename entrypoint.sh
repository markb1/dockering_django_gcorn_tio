#!/bin/sh

if [ "$DATABASE" = "postgres" ]; then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

if [ "$SQL_DATABASE" = "hello_django_dev" ]; then
   echo "Now running Django flush & DB migration"
   python manage.py flush --no-input
   python manage.py migrate
fi

exec "$@"
