### Project Setup

#### Create a new Django project

Create a new project directory, and a new Django "project":

``` shell
  PROJ_DIR=django_on_docker_tio/

  mkdir ${PROJ_DIR} && cd ${PROJ_DIR}
  mkdir app && cd app

  mkvirtualenv -p $(which python3) ${PROJ_DIR} .
  workon ${PROJ_DIR}

  # activated virtual-env!
  pip install django==2.2.6
  django-admin.py startproject hello_django .
  python manage.py migrate
  python manage.py runserver

  pip freeze > requirements.txt

```
#### Create a Dockerfile & docker-compose.yml

``` shell
  
  vim Dockerfile

  vim docker-compose.yml
  
```

#### Create or modify "Django settings" for this app

* Create a ``.env.dev`` in the project-root.

* Update ``hello_django/settings.py`` to pick-up those settings


### Add Py client & Postgresql server

Edit: ``Dockerfile``; ``docker-compose.yml``;
``hello_django/settings.py`` (PgSQL DB config); ``.env.dev`` and ``requirements.txt``

#### Build the new image, start the two containers

``` shell

  # start 'web' and 'db' containers
  docker-compose up -d --build

  # then, run the DB migrations!
  docker-compose exec web python manage.py migrate --noinput

```

#### Check that default Django tables are created

``` shell

  docker-compose exec db psql --username=hello_django --dbname=hello_django_dev

  # in 'db' container -- list DBs

  hello_django_dev=#  \l

  hello_django_dev=#  \c hello_django_dev
  -- You are now connected to database "hello_django_dev" as user "hello_django".

  # describe 'hello_django_dev` table

  hello_django_dev=#  \dt

```

To check for created Docker *volume*:

``` shell

   docker volume list | egrep -i 'postgres_data|volume name'  # get the volume name
   
   docker volume inspect  django_on_docker_tio_postgres_data

```

